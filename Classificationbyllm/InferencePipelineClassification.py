import pandas as pd
from transformers import pipeline # , AutoTokenizer, AutoModelForSequenceClassification


class InferencePipelineClassification:
    def __init__(self, model):
        # self.model = model
        self.pl = pipeline("text-classification", model=model, top_k=1)

    def get_classification(self, comment):
        # tekenizer = AutoTokenizer.from_pretrained()

        output = self.pl(comment)

        output_df = pd.DataFrame(output)

        return output_df





