import openai
#from langchain_community.llms import OpenAI
#from langchain.llms import OpenAI


class OpenAiClassification:

    def set_open_params(self, model="gpt-3.5-turbo-1106", temperature=0.7, max_tokens=256, top_p=3,
                        frequency_penalty=0, presence_penalty=0,):

        openai_params = {}

        openai_params['model'] = model
        openai_params['temperature'] = temperature
        openai_params['max_tokens'] = max_tokens
        openai_params['top_p'] = top_p
        openai_params['frequency_penalty'] = frequency_penalty
        openai_params['presence_penalty'] = presence_penalty

        return openai_params

    def get_answer(self, params, message):
        response = openai.chat.completions.create(
            model=params['model'],
            messages=message,
            temperature=params['temperature'],
            max_tokens=params['max_tokens'],
            top_p=params['top_p'],
            frequency_penalty=params['frequency_penalty'],
            presence_penalty=params['presence_penalty'],
        )

        return response





