import googleapiclient.discovery as gdis
import googleapiclient.errors as gerr
import pandas as pd


class YoutubeApiCommentCollection:

    def commentcollecting(self, api_key, link):
        api_service_name = "youtube"
        api_version = "v3"
        dev_key = api_key

        vid = [x for x in link]
        vid = ''.join(vid[-11:])
        video_id = vid

        youtube = gdis.build(
            api_service_name, api_version, developerKey=dev_key
        )

        request = youtube.commentThreads().list(
            part="snippet",
            videoId=video_id,
            maxResults=100
        )

        response = request.execute()

        comment = []

        for item in response["items"]:
            comment.append(item['snippet']['topLevelComment']['snippet']["textOriginal"])

        return comment



