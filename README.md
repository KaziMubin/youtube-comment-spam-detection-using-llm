# Youtube comment classification using llm

### Intruduction
In this project a user can give a youtube link as input python will collect all the comments on that video and process them. Then user can choose a pre-trained LLM model from `huggingface` and give that as input. It will show the comment classification. It will be done in three ways. One with `transformer` based inference api, secondly transformer based model locally and other one is prompt engineering. Later this will also add big models, so user can give input of classes they want in the classification. Which will use prompt engineering to make this happen. Also I will fine-tune a small model with preffered classified data and publish it on `huggingface`.

### Technology
- Python 3.12
- LLM
- Tensorflow
- pytorch
- transformers

### Project Structure
In the root folder there will be main.py file. Which will be used for calling different parts of the projects. `Commentcollection` folder will have all the files and programs needed to collect and store the comments. `classificationbyllm` folder will hold couple of .py files each implementing a different way of classifying comments.