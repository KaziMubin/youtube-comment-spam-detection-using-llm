import pandas as pd
from datasets import load_dataset
import warnings
from sklearn.model_selection import train_test_split


def google_dataset():
    dataset = load_dataset('SetFit/hate_speech_offensive', split='train')
    df = dataset.to_pandas()

    temp = df.drop(['text'], axis=1)
    first = temp.idxmax(axis=1)
    print(first.tail(20))

    fir = []
    for index, row in temp.iterrows():
        if row.max() < 0.5:
            fir.append('neutral')
        else:
            fir.append(row.idxmax())

    df['label'] = fir

    df.drop(['toxicity', 'severe_toxicity', 'obscene', 'threat', 'insult', 'identity_attack', 'sexual_explicit'],
            axis=1, inplace=True)
    print(df.head(20))
    df.to_csv("E:\\machine learning\\Youtube Comment classification using llm\\dataset-generation\\google_train.csv")


def setfit_hatespeech():
    dataset = load_dataset('SetFit/hate_speech_offensive', split='test')
    df = dataset.to_pandas()

    df.drop(['label'], axis=1, inplace=True)
    print(df.head(20))
    df.to_csv("E:\\machine learning\\Youtube Comment classification using llm\\dataset-generation\\hate_speech_test.csv")


def posneg_sentiment():
    dataset = load_dataset('Sp1786/multiclass-sentiment-analysis-dataset', split='validation')
    df = dataset.to_pandas()

    df.drop(['label'], axis=1, inplace=True)
    print(df.head(20))
    df.to_csv(
        "E:\\machine learning\\Youtube Comment classification using llm\\dataset-generation\\posneg_val.csv")


def emotion():
    dataset = load_dataset('dair-ai/emotion', 'unsplit', split='train')

    dic = {
        0: "sadness",
        1: "joy",
        2: "love",
        3: "anger",
        4: "fear",
        5: "surprise"
    }

    df = dataset.to_pandas()
    df['label'] = df['label'].map(dic)

    print(df.head(20))
    df.to_csv("E:\\machine learning\\Youtube Comment classification using llm\\dataset-generation\\emotion.csv")


def inspiration():
    dataset = load_dataset('PradeepReddyThathireddy/Inspiring_Content_Detection_Dataset', split='test')
    df = dataset.to_pandas()

    dic = {0: "Neutral", 1: "inspirational"}
    df['label'] = df['label'].map(dic)

    df = df[df['label'] == 'inspirational'].reset_index()
    df.to_csv(
        "E:\\machine learning\\Youtube Comment classification using llm\\dataset-generation\\inspirational_test.csv")


def generate_custom_dataset(ds):
    uniq_val = ds['label'].unique()
    rows = []
    for label in uniq_val:
        if len(ds[ds.label == label]) < 2000:
            subset = ds[ds['label'] == label]
            for _, row in subset.iterrows():
                rows.append({'text': row['text'], 'label': row['label']})
        else:
            subset = ds[ds['label'] == label].sample(2000)
            for _, row in subset.iterrows():
                rows.append({'text': row['text'], 'label': row['label']})

    new_df = pd.DataFrame(rows)

    return new_df


def train_test_generation(dataset):
    x = dataset['text']
    y = dataset['label']

    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=40)

    train_df = {'text': x_train, 'label': y_train}
    test_df = {'text': x_test, 'label': y_test}

    dataset_train = pd.DataFrame(train_df, )
    dataset_test = pd.DataFrame(test_df)

    dataset_train.to_csv(".\\train.csv", index=False)
    dataset_test.to_csv(".\\test.csv", index=False)


def main():
    warnings.filterwarnings("ignore")
    # google_dataset()
    # setfit_hatespeech()
    # posneg_sentiment()
    # emotion()
    # inspiration()
    col_list = ['text', 'label']
    ds = pd.read_csv("E:\\machine learning\\Youtube Comment classification using llm\\dataset-generation\\emotion.csv")
    ds = ds[col_list]
    ds1 = pd.read_csv(
        "E:\\machine learning\\Youtube Comment classification using llm\\dataset-generation\\inspirational.csv")
    ds1 = ds1[col_list]
    ds2 = pd.read_csv(
        "E:\\machine learning\\Youtube Comment classification using llm\\dataset-generation\\hate_speech.csv")
    ds2 = ds2[col_list]

    df1 = generate_custom_dataset(ds)
    df2 = generate_custom_dataset(ds1)
    df3 = generate_custom_dataset(ds2)

    new_df = pd.concat([df1, df2], ignore_index=True)
    new_df = pd.concat([new_df, df3], ignore_index=True)
    print(new_df)

    new_df = new_df.sample(frac=1).reset_index(drop=True)
    new_df.to_csv(".\\new_dataset_shuffled.csv", index=False)

    train_test_generation(new_df)


if __name__ == "__main__":
    main()