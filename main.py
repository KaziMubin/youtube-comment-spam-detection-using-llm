import openai
import IPython
import CommentCollection.YoutubeApiCommentCollection as yc
import Classificationbyllm.OpenAiClassification as cl
import Classificationbyllm.InferencePipelineClassification as inf
from dotenv import load_dotenv
import os
import warnings


def openai(comment):
    load_dotenv()

    openai.api_key = os.getenv("OPENAI_API_KEY")

    classification = cl.OpenAiClassification()

    params = classification.set_open_params()

    prompt = f"""Classify the comment into spam or not-spam.

             Comment: {comment}

             Class:
             """
    message = [
        {
            "role": "user",
            "content": prompt
        }
    ]

    output = classification.get_answer(params, message)

    IPython.display.Markdown(output.choices[0].message.content)


def inferencepipeline(comment, model):
    cls = inf.InferencePipelineClassification(model)

    print(cls.get_classification(comment))


def main():

    # comment = "It’s really cool to see your passion be put into this video," \
    #           " Marques. It goes to show that when the host is genuinely excited," \
    #           " that excitement can easily be transferred onto the viewers."
    # yc.YoutubeApiCommentCollection.commentcollecting()
    warnings.filterwarnings("ignore")
    load_dotenv()
    api_key = os.getenv("GOOGLE_APPLICATION_CREDENTIALS")

    video_link = input("Please enter the video link:")
    com = yc.YoutubeApiCommentCollection()
    comment = com.commentcollecting(api_key, video_link)

    # openai(comment)
    user_model_name = input("Give the model name:")
    inferencepipeline(comment, user_model_name)


if __name__ == "__main__":
    main()